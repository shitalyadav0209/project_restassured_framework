Endpoint is :
https://reqres.in/api/users

RequestBody is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is :
Tue, 20 Feb 2024 14:20:33 GMT

ResponseBody is :
{"name":"morpheus","job":"leader","id":"170","createdAt":"2024-02-20T14:20:33.298Z"}