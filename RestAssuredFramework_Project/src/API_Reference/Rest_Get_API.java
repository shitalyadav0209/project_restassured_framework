package API_Reference;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class Rest_Get_API {

	public static void main(String[] args) {
		
		//Collect all needed information and save into local variables
		String req_body="";
		String hostname="https://reqres.in/";
		String resource="api/users?page=2";
		String headername="Content-Type";
		String headervalue="application/json";
		
		//Declare Base URI
		RestAssured.baseURI=hostname;
		
		//Configure the API for execution and save the response in a String variables
		String res_body=given().header(headername,headervalue).when().get(resource).then().
				              extract().response().asString();
		System.out.println(res_body);
		
		//Parse the response body
		
		//Step1.1: Create the object of jsonPath
		JsonPath jsp_res=new JsonPath(res_body);
		
		//Step1.2: Parse individual parameters using jsp_res object
		String res_id=jsp_res.getString("id");
		System.out.println(res_id);
		String res_email=jsp_res.getString("email");
		System.out.println(res_email);
		String res_fname=jsp_res.getString("first-name");
		System.out.println(res_fname);
		String res_lname=jsp_res.getString("last_name");
		System.out.println(res_lname);
		String res_avatar=jsp_res.getString("avatar");
		System.out.println(res_avatar);
		
		//Store the expected result
		String exp_id="[7,8,9,10,11,12]";
		String exp_email="[michael.lawson@reqres.in,lindsay.ferguson@reqres.in,tobias.funke@reqres.in,byron.fields@reqres.in,george.edwards@reqres.in,rachel.howell@reqres.in]";                                          
		String exp_fname="[Michael,Lindsay,Tobias,Byron,George,Rachel]";
		String exp_lname="[Lawson,Ferguson,Funke,Fields,Edwards,Howell]";
		String exp_avatar="[https://reqres.in/img/faces/7-image.jpg,https://reqres.in/img/faces/8-image.jpg,https://reqres.in/img/faces/9-image.jpg,https://reqres.in/img/faces/10-image.jpg,https://reqres.in/img/faces/11-image.jpg,https://reqres.in/img/faces/12-image.jpg]";
		
		//Use TestNg Assert
		Assert.assertEquals(res_id, exp_id);
		Assert.assertEquals(res_email, exp_email);
		Assert.assertEquals(res_fname, exp_fname);
		Assert.assertEquals(res_lname, exp_lname);
		Assert.assertEquals(res_avatar, exp_avatar);
		
	}

}


	


