package API_Reference;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class Rest_Patch_API {

	public static void main(String[] args) {
		
		// Collect all needed information and save it into local variables
		String req_body= "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}";
		String hostname="https://reqres.in/";
		String resource="api/users/2";
		String headername="Content-Type";
		String headervalue="application/json";
		
		// Declare BaseURI
		RestAssured.baseURI=hostname;
		
		//Configure the API for execution and the save the response in a string variable
		String res_body=given().header(headername,headervalue).body(req_body).when().patch(resource).then().
				            extract().response().asString();
		System.out.println(res_body);
		
		//Parse the response body
		//Step1.1: Create the object of JsonPath
		JsonPath jsp_res=new JsonPath(res_body);
		
		//Step1.2: parse individual parameters using jsp_res object
		String res_name=jsp_res.getString("name");
		System.out.println(res_name);
		String res_job=jsp_res.getString("job");
		System.out.println(res_job);
		String res_updatedAt=jsp_res.getString("updatedAt");
		res_updatedAt=res_updatedAt.substring(0,10);
		System.out.println(res_updatedAt);
		
		//Validate the response body
		//Step2.1: Parse the request body and save it into local variables
		JsonPath jsp_req=new JsonPath(req_body);
		String req_name=jsp_req.getString("name");
		String req_job=jsp_req.getString("job");
		
		//Step2.2: Generate expected Date
		LocalDateTime CurrentDate=LocalDateTime.now();
		String expecteddate=CurrentDate.toString().substring(0,10);
		
		//Step2.3: Use TestNg Assert
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, expecteddate);
		

	}

}
