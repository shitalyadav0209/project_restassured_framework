package Common_Method;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.apache.poi.ss.usermodel.CellType;

public class Utility {
	
	
	public static ArrayList<String> readExcelData(String SheetName,String TestCase) throws IOException{
		
		ArrayList<String> arrayData=new ArrayList<String>();


		
		//Step1:Fetch the Java project name and location
		String projectDir=System.getProperty("user.dir");
	//	System.out.println("Current project Directory is:" +projectDir);
		
		//Step2: Create an object of File Input Stream to locate the excel file
		FileInputStream fis=new FileInputStream(projectDir + "\\DataFiles\\Input_Data.xlsx");
		
		//Step3: Create an object of XSSFWorkbook to open the excel file
		
		//Step3.1: Fetch the count of sheets
		XSSFWorkbook wb=new XSSFWorkbook(fis);
		int count=wb.getNumberOfSheets();
//		System.out.println("Count of sheets is:" +count);
		
		//Step4: Access the desired sheet
		for(int i=0; i<count; i++) {
			if(wb.getSheetName(i).equals(SheetName)) {
	//			System.out.println("Sheet at index" +i+ ":" +wb.getSheetName(i));
				
				//Step4.1: Access the row data from sheet
				XSSFSheet datasheet=wb.getSheetAt(i);
				Iterator<Row> rows=datasheet.iterator();
				String testcasefound="False";
				while(rows.hasNext()) {
					Row datarows=rows.next();
					String tcname=datarows.getCell(0).getStringCellValue();
					if(tcname.equals(TestCase)) {
						 testcasefound="True";
						
						//Step4.2: Fetch the cell data from row
						Iterator<Cell> cellvalues=datarows.cellIterator();
						while(cellvalues.hasNext()) {
							Cell Cell= cellvalues.next();
							String testdata="";
							
							//method 1: Handle multiple data type 
					/*		try {
								testdata=Cell.getStringCellValue();
							}
							catch(IllegalStateException e){
								double inttestdata=Cell.getNumericCellValue();
								testdata=String.valueOf(inttestdata);
							}     */
							
							//Method 2: Handle multiple data type
							CellType dataType=Cell.getCellType();
							if(dataType.toString().equals("STRING")) {
								testdata=Cell.getStringCellValue();
							}
							else if(dataType.toString().equals("NUMERIC")) {
									double inttestData=Cell.getNumericCellValue();
									testdata=String.valueOf(inttestData);
								}
							
					//		System.out.println(testdata);
							
							//Step4.3: Add data in arrayList
							arrayData.add(testdata);
						}
						break;
					}
					
				}
				if(testcasefound.equals("False")) {
					System.out.println(TestCase + "test case not found in sheet:" +wb.getSheetName(i));
					
				}
				break;
			}    
			else {
				System.out.println(SheetName + "sheet not found in file Input_Data.xlsx at index:" +i);
			}
		}
		wb.close();     
		return arrayData;
	}
				
	
	public static void evidenceFileCreator(String FileName, File FileLocation, String endpoint, String RequestBody, String ResHeader, String ResponseBody) throws IOException {
		
		 //Create and Open the file
		File NewTextFile=new File(FileLocation + "\\" + FileName + ".txt");
//		System.out.println("File create with name:" + NewTextFile.getName());
		
		//Step 2: Write data
		FileWriter writedata=new FileWriter(NewTextFile);
		writedata.write("Endpoint is :\n" + endpoint + "\n\n");
		writedata.write("RequestBody is :\n" + RequestBody + "\n\n");
		writedata.write("Response header date is :\n" + ResHeader + "\n\n");
		writedata.write("ResponseBody is :\n" +ResponseBody);
		
		//Step 3: Save and Close
		writedata.close();
		
	}
	
	public static File CreateLogDirectory(String dirName) {
		
		//Step1: Fetch the java project name and location
		String projectDir=System.getProperty("user.dir");
	//	System.out.println("Current project dirctory is:" +projectDir);
		
		//Step2: Verify whether the directory in variable dirName exists inside the projectDir and act accordingly
		
		File dirctory=new File(projectDir +"\\"+ dirName);
		
		if(dirctory.exists()) {
	//		System.out.println(dirctory +" , already exists");
		}
		else
		{
		//	System.out.println(dirctory + " , doesnot exists , hence created it");
			dirctory.mkdir();
	//		System.out.println(dirctory + " , created");
		}
		return dirctory;
	
}
	
	public static String testLogName(String Name) {
		
		LocalTime currentTime=LocalTime.now();
		String currentStringTime=currentTime.toString().replaceAll(":", "");
		String TestLogName="TC_1" +currentStringTime;
		return TestLogName;
	     

	}
	}


