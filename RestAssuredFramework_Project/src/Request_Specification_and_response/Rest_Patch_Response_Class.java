package Request_Specification_and_response;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Rest_Patch_Response_Class {

	public static void main(String[] args) {
		
		//Step1: Collect all needed information and save it into local variable
		String req_body="{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}";
		String hostname="https://reqres.in/";
		String resource="api/users/2";
		String headername="Content-Type";
		String headervalue="application/json";
		
		//Step 2: Build the Request specification using RequestSpecification Class
				RequestSpecification requestSpec=RestAssured.given();
				
				//Step2.1: Set Request header
				requestSpec.header(headername,headervalue);
				
				//Step2.2: Set request body
				requestSpec.body(req_body);
				
				//Step3: Send the API request
				Response response=requestSpec.patch(hostname+resource);
				
				//Step4: Parse the response body
				ResponseBody res_body=response.getBody();
				
			    String res_name=res_body.jsonPath().getString("name");
			    System.out.println(res_name);
			    String res_job=res_body.jsonPath().getString("job");
			    System.out.println(res_job);
			    String res_updatedAt=res_body.jsonPath().getString("updatedAt");
			    res_updatedAt=res_updatedAt.substring(0,10);
			    System.out.println(res_updatedAt);
			    
			    //Step5: Validate the response body
			    
			    //Step5.1: parse request body and save into local variables
			    JsonPath jsp_req=new JsonPath(req_body);
			    String req_name=jsp_req.getString("name");
			    String req_job=jsp_req.getString("job");
			    
			    //Step5.2: Generate expected Date
			    LocalDateTime CurrentDate=LocalDateTime.now();
			    String expecteddate=CurrentDate.toString().substring(0,10);
			    
			    //Step5.3: Use TestNg Assert
			    Assert.assertEquals(res_name, req_name);
			    Assert.assertEquals(res_job, req_job);
			    Assert.assertEquals(res_updatedAt, expecteddate);
			    

	}

}


	
