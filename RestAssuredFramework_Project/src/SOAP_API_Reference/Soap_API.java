package SOAP_API_Reference;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;

public class Soap_API {

	public static void main(String[] args) {
		
		//Step1: Collect all needed information and save it into local variables
				RestAssured.useRelaxedHTTPSValidation();
				String req_body="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n"
						+ "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n"
						+ "  <soap:Body>\r\n"
						+ "    <NumberToWords xmlns=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
						+ "      <ubiNum>43</ubiNum>\r\n"
						+ "    </NumberToWords>\r\n"
						+ "  </soap:Body>\r\n"
						+ "</soap:Envelope>\r\n"
						+ "";
				String hostname="https://www.dataaccess.com";
				String resource="/webservicesserver/NumberConversion.wso";
				String headername="Content-Type";
				String headervalue="text/xml;charset=utf-8";
				
				//Step2: Declare BaseURI
				RestAssured.baseURI=hostname;
				
				//Step3: Configure the API for execution and save the response in a String Variable
				String res_body=given().header(headername,headervalue).body(req_body).when().post(resource).
						                then().extract().response().getBody().asString();
				System.out.println(res_body);
				
				//Step4: Parse the response body
				
				//Step4.1: Create the object of JsonPath
				XmlPath xml_res=new XmlPath(res_body);
				
				//Step4.2: Parse individual parameters using xml_res object
				String result=xml_res.getString("NumberToWordsResult");
				
				//Step5: Validate the response body
				Assert.assertEquals(result, "forty three dollars");
				
				

			}

		}


	
